﻿namespace TimePicker
{
    partial class TimePicker
    {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージド リソースを破棄する場合は true を指定し、その他の場合は false を指定します。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region コンポーネント デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を 
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.flowLayoutPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.datePicker = new System.Windows.Forms.DateTimePicker();
            this.hourBox = new System.Windows.Forms.ComboBox();
            this.minutesBox = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // flowLayoutPanel
            // 
            this.flowLayoutPanel.AutoSize = true;
            this.flowLayoutPanel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.flowLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanel.Name = "flowLayoutPanel";
            this.flowLayoutPanel.Size = new System.Drawing.Size(200, 20);
            this.flowLayoutPanel.TabIndex = 0;
            // 
            // datePicker
            // 
            this.datePicker.Dock = System.Windows.Forms.DockStyle.Left;
            this.datePicker.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.datePicker.Location = new System.Drawing.Point(0, 0);
            this.datePicker.MinimumSize = new System.Drawing.Size(121, 20);
            this.datePicker.Name = "datePicker";
            this.datePicker.RightToLeftLayout = true;
            this.datePicker.Size = new System.Drawing.Size(121, 20);
            this.datePicker.TabIndex = 1;
            this.datePicker.ValueChanged += new System.EventHandler(this.DatePicker_ValueChanged);
            // 
            // hourBox
            // 
            this.hourBox.DisplayMember = "00";
            this.hourBox.Dock = System.Windows.Forms.DockStyle.Left;
            this.hourBox.FormattingEnabled = true;
            this.hourBox.Items.AddRange(new object[] {
            "00",
            "01",
            "02",
            "03",
            "04",
            "05",
            "06",
            "07",
            "08",
            "09",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20",
            "21",
            "22",
            "23"});
            this.hourBox.Location = new System.Drawing.Point(121, 0);
            this.hourBox.MaxLength = 2;
            this.hourBox.MinimumSize = new System.Drawing.Size(36, 0);
            this.hourBox.Name = "hourBox";
            this.hourBox.Size = new System.Drawing.Size(36, 20);
            this.hourBox.TabIndex = 2;
            this.hourBox.Text = "00";
            this.hourBox.ValueMember = "00";
            this.hourBox.SelectedIndexChanged += new System.EventHandler(this.HourBox_SelectedIndexChanged);
            // 
            // minutesBox
            // 
            this.minutesBox.Dock = System.Windows.Forms.DockStyle.Left;
            this.minutesBox.FormattingEnabled = true;
            this.minutesBox.Items.AddRange(new object[] {
            "00",
            "01",
            "02",
            "03",
            "04",
            "05",
            "06",
            "07",
            "08",
            "09",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20",
            "21",
            "22",
            "23",
            "24",
            "25",
            "26",
            "27",
            "28",
            "29",
            "30",
            "31",
            "32",
            "33",
            "34",
            "35",
            "36",
            "37",
            "38",
            "39",
            "40",
            "41",
            "42",
            "43",
            "44",
            "45",
            "46",
            "47",
            "48",
            "49",
            "50",
            "51",
            "52",
            "53",
            "54",
            "55",
            "56",
            "57",
            "58",
            "59"});
            this.minutesBox.Location = new System.Drawing.Point(157, 0);
            this.minutesBox.MinimumSize = new System.Drawing.Size(36, 0);
            this.minutesBox.Name = "minutesBox";
            this.minutesBox.Size = new System.Drawing.Size(36, 20);
            this.minutesBox.TabIndex = 3;
            this.minutesBox.Text = "00";
            this.minutesBox.SelectedIndexChanged += new System.EventHandler(this.MinutesBox_SelectedIndexChanged);
            // 
            // TimePicker
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.Controls.Add(this.minutesBox);
            this.Controls.Add(this.hourBox);
            this.Controls.Add(this.datePicker);
            this.Controls.Add(this.flowLayoutPanel);
            this.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.MinimumSize = new System.Drawing.Size(200, 20);
            this.Name = "TimePicker";
            this.Size = new System.Drawing.Size(200, 20);
            this.Resize += new System.EventHandler(this.TimePicker_Resize);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel;
        public System.Windows.Forms.DateTimePicker datePicker;
        public System.Windows.Forms.ComboBox hourBox;
        public System.Windows.Forms.ComboBox minutesBox;
    }
}
