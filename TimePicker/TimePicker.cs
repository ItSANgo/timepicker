﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace TimePicker
{
    public partial class TimePicker : UserControl
    {
        [
        Category("Behavier"),
        Description("")
        ]
        public DateTime Value
        {
            get => GetCurrentValue();
            set
            {
                if (value > DateTimePicker.MaximumDateTime)
                {
                    value = DateTimePicker.MaximumDateTime;
                }
                else if (value < DateTimePicker.MinimumDateTime)
                {
                    value = DateTimePicker.MinimumDateTime;
                }

                if (value != datePicker.Value)
                {
                    datePicker.Value = value;
                }
                if (value.Hour != hourBox.SelectedIndex)
                {
                    hourBox.SelectedIndex = value.Hour;
                }
                if (value.Minute != minutesBox.SelectedIndex)
                {
                    minutesBox.SelectedIndex = value.Minute;
                }
            }
        }

        public event EventHandler ValueChanged
        {
            add
            {
                onValueChanged += value;
            }
            remove
            {
                onValueChanged -= value;
            }
        }

        public TimePicker()
        {
            InitializeComponent();
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            e.Graphics.DrawString(Text, Font, new SolidBrush(ForeColor), ClientRectangle);
        }

        private EventHandler onValueChanged;

        private void DatePicker_ValueChanged(object sender, EventArgs e)
        {
            CommonValueChangedLogic(sender, e);
        }

        private void HourBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            CommonValueChangedLogic(sender, e);
        }

        private void MinutesBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            CommonValueChangedLogic(sender, e);
        }

        private void CommonValueChangedLogic(object sender, EventArgs e)
        {
            DateTime newValue = GetCurrentValue();
            if (newValue != Value)
            {
                Value = newValue;
                onValueChanged?.Invoke(sender, e);
            }
        }

        private DateTime GetCurrentValue()
        {
            DateTime newValue = datePicker.Value.AddHours(-datePicker.Value.Hour + hourBox.SelectedIndex);
            return newValue.AddMinutes(value: -newValue.Minute + minutesBox.SelectedIndex);
        }

        private const int MinX = 200;
        private const int MinY = 20;
        private const double MinWidthForDatePicker = 121d;
        private const double MinWidthForBoxes = 36d;

        private void TimePicker_Resize(object sendr, EventArgs e)
        {
            var size = this.Size;
            var x = size.Width;
            var y = size.Height;
            if (x < MinX)
            {
                x = MinX;
            }
            if (y < MinY)
            {
                y = MinY;
            }
            var denominator = MinX;
            var widthForDatePicker = (MinWidthForDatePicker / denominator) * x;
            if (widthForDatePicker < MinWidthForDatePicker)
            {
                widthForDatePicker = MinWidthForDatePicker;
            }
            var widthForBoxes = (MinWidthForBoxes / denominator) * x;
            if (widthForBoxes < MinWidthForBoxes)
            {
                widthForBoxes = MinWidthForBoxes;
            }
            flowLayoutPanel.Size = this.Size;
            flowLayoutPanel.Location = this.Location;
            datePicker.Size = new Size((int)widthForDatePicker, y);
            hourBox.Size = new Size((int)widthForBoxes, y);
            hourBox.Location = new Point((int)(widthForDatePicker + datePicker.Margin.Right + hourBox.Margin.Left), 0);
            minutesBox.Size = hourBox.Size;
            minutesBox.Location = new Point((int)(hourBox.Location.X + widthForBoxes + hourBox.Margin.Right + minutesBox.Margin.Left), 0);

            var graphics = this.CreateGraphics();
            var newEventArgs = new PaintEventArgs(graphics, new Rectangle(this.Location, this.Size));
            OnPaint(newEventArgs);
            graphics.Dispose();
        }
    }

}
